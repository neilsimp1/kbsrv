use clap::{Arg, ArgAction, Command};
use std::ffi::OsStr;

pub fn cli() -> Command {
	Command::new("kbsrv")
		.about("Send keys from one PC to another")
		.version("0.1.0")
		.subcommand_required(true)
		.arg_required_else_help(true)

		.subcommand(
			Command::new("client")
				.short_flag('c')
				.long_flag("client")
				.about("Send a key to another machine.")
				.arg(
					Arg::new("host")
						.short('s')
						.long("host")
						.help("Hostname or IP address to connect to.")
						.action(ArgAction::Set),
				)	
				.arg(
					Arg::new("port")
						.short('p')
						.long("port")
						.help("Specify what port to listen on. Default 8369.")
						.action(ArgAction::Set)
						.default_value(&OsStr::new("8369")),
				)	
				.arg(
					Arg::new("keys")
						.help("Keys to send to server, space-separated.")
						.required(true)
						.action(ArgAction::Set)
						.num_args(1..),
				)
		)

		.subcommand(
			Command::new("daemon")
				.short_flag('d')
				.long_flag("daemon")
				.about("Listen for input to run on the keyboard.")
				.arg(
					Arg::new("port")
						.short('p')
						.long("port")
						.help("Specify what port to listen on.")
						.action(ArgAction::Set)
						.default_value(&OsStr::new("8369")),
				)
		)

		.subcommand(
			Command::new("showkeys")
				.short_flag('k')
				.long_flag("showkeys")
				.about("Print list of keycodes used (/usr/include/linux/input-event-codes.h).")
		)
}
