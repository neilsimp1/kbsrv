mod cli;

use cli::cli;
use std::fs;
use std::{
	io::{prelude::*, BufReader},
	net::{TcpListener, TcpStream},
	process::Command,
};
use sysinfo::System;

const INPUT_EVENT_CODES_PATH: &str = "/usr/include/linux/input-event-codes.h";

fn main() -> std::io::Result<()> {
	let matches = cli().get_matches();

	match matches.subcommand() {
		Some(("client", client_matches)) => {
			let host = client_matches
				.get_one::<String>("host")
				.expect("is present");
			let port = client_matches
				.get_one::<String>("port")
				.expect("is present");
			let keys = client_matches
				.get_many::<String>("keys")
				.expect("is present")
				.map(|key| key.as_str())
				.collect::<Vec<_>>()
				.join(" ");
			
			client_mode(host, port, &keys);

			Ok(())
		}
		
		Some(("daemon", daemon_matches)) => {
			let port = daemon_matches
				.get_one::<String>("port")
				.expect("is present");
			
			if is_ydotoold_running() {
				server_mode(port);
				Ok(())
			}
			else {
				panic!("Please check if ydotoold is running.")
			}
		}
		
		Some(("showkeys", _)) => {
			show_keys();
			Ok(())
		}

		_=> unreachable!(),
	}
}

fn client_mode(host: &String, port: &String, keys: &String) {
	let mut stream = TcpStream::connect(format!("{host}:{port}")).expect("Cannot connect");
	stream.write(keys.as_bytes()).expect("Cannot connect");
}

fn server_mode(port: &String) {
	let listener = TcpListener::bind(format!("0.0.0.0:{port}")).unwrap();
	for stream in listener.incoming() {
		handle_connection(stream.unwrap());
	}
}

fn show_keys() {
	let contents = fs::read_to_string(INPUT_EVENT_CODES_PATH)
		.expect("Unable to find input-event-codes.h!");

	println!("{}", contents);
}

fn handle_connection(mut stream: TcpStream) {
	let mut buf_reader = BufReader::new(&mut stream);
	let mut request = String::new();
	let _ = buf_reader.read_line(&mut request);
	let key_codes = request.split(" ");

	Command::new("ydotool")
		.arg("key")
		.args(key_codes)
		.spawn()
		.expect("Failed to execute ydotool.");
}

fn is_ydotoold_running() -> bool {
	let s = System::new_all();
	for _process in s.processes_by_exact_name("ydotoold") {
		return true
	}
	false
}
