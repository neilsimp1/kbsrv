# kbsrv

This is a simple tool mean in two parts.
Run this in server mode (`-d`) to create a thin TCP wrapper around the [ydotool](https://github.com/ReimuNotMoe/ydotool) `key` command.
Run this in client mode to issue `ydotool key` commands to the remote server.

This is meant to scratch a personal itch but may be useful in other ways. This program is not secure and shouldn't be exposed to untrusted clients.

## Dependencies
This program requires `ydotool` be installed and `ydotoold` be running in order to run in server mode.

## How to build
```
cargo build
```

## How to run
As a client, send a single <SPACE> key to the server:
```
kbsrv -c -s 127.0.0.1 57:1 57:0
```

As a server:
```
kbsrv -d
```
